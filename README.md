# Spike Report

## GRAPHICS PROGRAMMING - Indoor Lighting

### Introduction

In this spike we will learn how to set up lighting in a controlled indoor environment.

### Goals

- A small indoor scene, with light coming in from the windows, and a three-point lighting setup for a high-poly �feature� model placed upon a table in the middle of the room (get one online).
    - Start using the Lighting Quick Start Guide
    - Have most objects Static
    - Have the feature object Movable (so the player can knock it around by walking into it).
    - Use at least one Point Light and one Spot Light
    - Have at least one smooth and/or metallic object, and use a Reflection Capture (box or sphere) to aid in reflection.
    - Use a Light Profile on as many lights as plausible.
    - For the windows, create two copies of the level.
    - One will use light-cards
    - One will use Lightmass portals


### Personnel

* Primary - Mitchell Bailey
* Secondary - Bradley Eales

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Lighting Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/QuickStart/1/index.html)
* [High-Poly Model](https://www.cgtrader.com/items/773837/download-page)
* [How To Set Up 3 Point Lighting](http://www.kembadesign.com/tutorials/2016/6/10/how-to-set-up-three-point-lighting-in-unreal-engine-412)
* [Simulating Area Lights (Light Cards)](https://www.unrealengine.com/en-US/blog/simulating-area-lights-in-ue4)

### Tasks Undertaken

1. Completed the Lighting Quick Start Guide.
1. Downloaded the high-poly model and place somewhere in scene.
1. Set up 3 point lighting system on the model.
    - Placed key light (point light) to the front-left of the model.
    - Placed fill light (spot light) to the front-right of the model.
        - Angled fill light to face the model.
    - Placed back light (point light) to the rear-right of the model.
    - Adjusted attentuation radius of each light to eliminate lighting overlap errors.
1. Placed multiple point lights in small back room with different light profiles to compare.
1. Gave a 400 x 400 wall starter content model a metallic material, also found in starter content.
    - Resized wall appropriately and placed a reflection box capture in front of it to reflect the room.
1. Placed lightmass portals in all doors and windows.
1. Duplicated level and replaced lightmass portals with light cards (refer to resources for help).

### What we found out

- Lightmass portals help light filter in through windows, doors, any type of light opening, whereas, light cards create a 'false' light to help project light into those openings. An assumption based on this knowledge would be that the lightmass portals and light cards could be used together to create the most realistic lighting.

- 3 point lighting systems are lighting methods and their purpose is to properly illuminate a subject in an effective way that is not only pleasing to the eyes but also relatively simple in its approach. When using the 3 point lighting method in a game engine, it would be most commonly used in cinematics or cutscenes to highlight the characters and have some sort of natural lighting in the world.

### Open Issues/Risks

- 3 point lighting systems are tricky to set up as it is easy to cause overlapping light errors, cancelling the effects of the overlapped light.

- Was unable to complete the moveable object for the player to knock around the scene. Would suggest moving to another spike to be completed.

### Recommendations

- A high amount of skill is needed to correctly set up a 3 point lighting system.